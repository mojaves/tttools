#!/usr/bin/env python3
#
# elo.py is (C) 2022 Francesco Romani fromani/gmail
# SPDX short identifier: MIT
# License: MIT

import argparse
import collections
import csv
import json
import logging
import operator
import os.path
import sys

_Match = collections.namedtuple('_Match', ['round', 'player', 'player_score', 'opponent', 'opponent_score'])

MATCH_POINTS_SUM = 20

class Match(_Match):

    @classmethod
    def make(cls, items):
        round_, player, player_score_, opponent, opponent_score_ = items
        return cls(int(round_), player.strip().lower(), int(player_score_), opponent.strip().lower(), int(opponent_score_))

    def to_csv(str):
        return ",".join(self._fields)

    def to_str(self):
        return "round=%d: [%24s] vs [%24s]: %2s - %2s (winner=%s)" % (
                self.round,
                self.player, self.opponent, self.player_score, self.opponent_score,
                self.winner())

    def validate(self, allowed_sum=None):
        if allowed_sum is not None:
            if (self.player_score + self.opponent_score) != allowed_sum:
                raise ValueError("wrong scoring: %d + %d != %d" % (self.player_score, self.opponent_score, allowed_sum))

    def winner(self):
        winner, loser, drawn = self.result()
        return None if drawn else winner

    def result(self):
        if self.player_score == self.opponent_score:
            return self.player, self.opponent, True
        if self.player_score > self.opponent_score:
            return self.player, self.opponent, False
        return self.opponent, self.player, False



_Tournament = collections.namedtuple('_Tournament', ['date', 'where', 'name', 'rank', 'matches'])

class Tournament(_Tournament):

    @classmethod
    def make(cls, filename):
        date, where, name, rank = _parse_filename(filename)
        with open(filename, "r") as src:
            lines = list(csv.reader(src))
        matches = list(sorted(map(Match.make, lines[1:]), key=operator.attrgetter('round')))
        return cls(date, where, name, rank, matches)

    def validate(self, allowed_sum=None):
        for m in self.matches:
            m.validate(allowed_sum)

    def to_str(self):
        return "{%s} %s - %s - rank=%d\n%s" % (
                self.name, self.where, self.date, self.rank,
                '\n'.join(m.to_str() for m in self.matches)
        )


def _parse_filename(name):
    date, rank_, where, name = os.path.splitext(os.path.basename(name))[0].split('-')
    return (date, where, name, int(rank_[1:]))

WIN = 1.
DRAW = 0.5
LOSS = 0.
F_FACTOR = 400.
BASERATING = 2000.

class Elo:
    def __init__(self, k=20, g=1, baserating=BASERATING, ffactor=F_FACTOR):
        self._ratings = {}	
        self._k = k
        self._g = g
        self._baserating = baserating
        self._ffactor = ffactor

    def params(self):
        return {
                "K": self._k,
                "G": self._g,
                "F_factor": self._ffactor,
                "baserating": self._baserating,
        }

    def load(self, filename):
        loaded = 0
        with open(filename, "r") as src:
            for entry in json.load(src):
                self.load_player(entry["player"], int(entry["rating"]))
                loaded += 1
        logging.info(" - loaded %d players from %s", loaded, filename)

    def _dump_ladder(self):
        return [ { "player": k, "rating": round(v, 0) } for k, v in sorted(self._ratings.items(), key=operator.itemgetter(1), reverse=True) ]

    def dump(self, filename):
        ld = self._dump_ladder()
        with open(filename, "w") as dst:
            json.dump(ld, dst)

    def dumps(self):
        ld = self._dump_ladder()
        return json.dumps(ld, indent=4)

    def init_player(self, name, rating=None):
        if name in self._ratings:
            return
        self._ratings[name] = self._baserating

    def load_player(self, name, rating):
        self._ratings[name] = rating
		
    def add_result(self, winner, loser, draw=False, k=None, g=None):
        if draw and self._ratings[winner] > self._ratings[loser]:
            winner, loser = loser, winner
        new_winner, new_loser = self.compute(self._ratings[winner], self._ratings[loser], draw=draw, k=k)
        self._ratings[winner] = new_winner
        self._ratings[loser] = new_loser
		
    def compute(self, winner, loser, draw=False, k=None):
        scores = (DRAW, DRAW) if draw else (WIN, LOSS)
        return (
            self.rate(winner, [(scores[0],  loser)], k),
            self.rate(loser,  [(scores[1], winner)], k),
        )

    def rate(self, rating, series, k=None):
        K = self._k if k is None else k
        return float(rating) + k * self.adjust(rating, series)

    def adjust(self, rating, series):
        return sum(score - self.expect(rating, other_rating) for score, other_rating in series)

    def expect(self, rating, other_rating):
        # http://www.chess-mind.com/en/elo-system
        diff = float(other_rating) - float(rating)
        return 1. / (1 + 10 ** (diff / self._ffactor))


def k_from_rank(rank):
    return 20 if rank == 1 else 16


def setup_tournament(elo, T):
    for M in T.matches:
        elo.init_player(M.player)
        elo.init_player(M.opponent)


def process_tournament(elo, T):
    K = k_from_rank(T.rank)
    for M in T.matches:
        winner, loser, draw = M.result()
        logging.info(" - %2d [%24s] VS [%24s] %s" %(M.round, winner, loser, "TIE" if draw else "WIN " + winner))
        elo.add_result(winner, loser, draw=draw, k=K)


if __name__ == "__main__":
    logging.basicConfig(encoding='utf-8', format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

    EXE = "elo.py"
    logging.info("* %s starting up", EXE)

    parser = argparse.ArgumentParser()
    parser.add_argument("tournaments", type=str, nargs='+', help="tournaments CSVs to process")
    parser.add_argument("-L", "--ladder", type=str)
    args = parser.parse_args()

    elo = Elo()
    if args.ladder is not None:
        logging.info("+ loading ladder from: %s", args.ladder)
        elo.load(args.ladder)

    tourns = []
    for name in args.tournaments:
        logging.info("+ loading tournament from: %s", name)
        T = Tournament.make(name)

        logging.info("+ validating: %s", T.name)
        T.validate(MATCH_POINTS_SUM)

        logging.info("+ setting up: %s", T.name)
        setup_tournament(elo, T)

        logging.info("+ processing: %s", T.name)
        process_tournament(elo, T)

    print(elo.dumps())

    logging.info("* %s finished", EXE)
